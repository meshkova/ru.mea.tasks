package ru.mea.task_10;
import java.util.Scanner;

public class Task10 {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {
        System.out.println("Введите выражение:");
        String self = scanner.nextLine();

        String resulf = self.replaceAll("[\\s ! , . - _]", "");
        System.out.println(resulf);
        StringBuilder buffer = new StringBuilder(resulf);
        buffer.reverse();
        String answer = String.valueOf(buffer);

        boolean retVal;
        retVal = resulf.equalsIgnoreCase(answer);
        System.out.println(" " + retVal);
    }
}
