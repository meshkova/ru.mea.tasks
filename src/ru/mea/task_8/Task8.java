package ru.mea.task_8;


public class Task8 {
    public static void main(String[] args) {
        int[][] twoDimArray = new int[][]{{5, 7, 3, 17}, {7, 0, 1, 12}, {8, 1, 2, 3}};
        two(1, twoDimArray);
    }

  private static void two(int num, int[][] twoDimArray) {
        for (int i = 0; i < twoDimArray.length; i++) {  //идём по строкам
            for (int j = 0; j < twoDimArray[0].length; j++) {//идём по столбцам
                if (j != num) {
                    System.out.print(" " + twoDimArray[i][j] + " ");
                } else {
                    twoDimArray[i][j] = 0;

                }
            }
            System.out.println();
        }

    }
}


