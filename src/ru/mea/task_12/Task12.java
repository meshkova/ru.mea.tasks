package ru.mea.task_12;


public class Task12 {
    public static void main(String[] args) {
        int[][] twoDimArray = new int[7][4];
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                twoDimArray[i][j] = ((int) (Math.random() * 11) - 5);
                System.out.print(" " + twoDimArray[i][j] + " ");
            }
            System.out.println();
        }
        int c = twoDimArray.length * twoDimArray[0].length;
        int[] oneArray = new int[c];
        arrays(twoDimArray, oneArray);
        System.out.println("Вывод одномерного массива");
          for (int i = 0; i < oneArray.length; i++) {
            System.out.println(oneArray[i]);
        }
    }

    private static void arrays(int[][] twoDimArray, int[] oneArray) {
        int mas;
        int index = 0;
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                while (index < oneArray.length) {
                    mas = twoDimArray[i][j];
                    oneArray[index] = mas;
                    index++;
                    break;
                }
            }
        }

    }
}
