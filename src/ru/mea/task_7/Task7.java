package ru.mea.task_7;

import java.util.Scanner;

public class Task7 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        double num;
        System.out.println("Введите число");
        num = scanner.nextDouble();
        if (num % 1 == 0) {
            System.out.println("Целое");
        } else {
            System.out.println("Не целое");
        }
    }
}
