package ru.mea.task_14;
/**
  В квадратной матрице [n][n] поменять столбцы и строки местами.
 */

import java.util.Scanner;

public class Task14 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int[][] twoDimArray = new int[4][4];
         /*
        Заполнила матрицу
         */
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                twoDimArray[i][j] = ((int) (Math.random() * 11) - 5);
            }
        }
         /*
        Вывела матрицу
         */
        for (int i = 0; i < twoDimArray.length; i++) {
            int[] ints = twoDimArray[i];
            for (int j = 0; j < twoDimArray[0].length; j++) {
                System.out.print(" " + ints[j] + " ");
            }
            System.out.println();

        }
        System.out.println("Поменяла столбцы и строки местами");
        twoArray(twoDimArray);
    }

    private static void twoArray(int[][] twoDimArray) {
        /*
        Транспонировала матрицу
         */
        for (int i = 0; i < twoDimArray.length; i++) {
            for (int j = i + 1; j < twoDimArray[0].length; j++) {
                int trans = twoDimArray[i][j];
                twoDimArray[i][j] = twoDimArray[j][i];
                twoDimArray[j][i] = trans;
            }
        }
/*
Вывела матрицу
 */
        for (int[] ints : twoDimArray) {
            for (int j = 0; j < twoDimArray[0].length; j++) {
                System.out.print(" " + ints[j] + " ");
            }
            System.out.println();

        }
    }
}