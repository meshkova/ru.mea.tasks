package ru.mea.task_6;

import java.util.Scanner;

public class Task6 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int number;
        System.out.println("Введите число");
        number = scanner.nextInt();
        for (int i = 1; i <= 10; i++) {
            int num = number * i;
            System.out.println(num);

        }
    }
}
