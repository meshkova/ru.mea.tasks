package ru.mea.task_11;

import java.util.Scanner;

public class Task11 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        System.out.println("Введите количество суток");
        int day;
        day = scanner.nextInt();
        if (day >= 0) {
            int clock;
            clock = day * 24;
            int minutes = clock * 60;
            int seconds = minutes * 60;
            System.out.println("В " + day + "сутках = " + "часов " + clock + ", минут " + minutes + ", секунд " + seconds);
        } else {
            System.out.println("Столько суток не бывает");
        }
    }
}
