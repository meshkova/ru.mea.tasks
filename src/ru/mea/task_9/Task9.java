package ru.mea.task_9;


import java.util.Scanner;


public class Task9 {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {

        int num;
        System.out.println("Введите число");
        num = scanner.nextInt();
        int[] number = new int[num];

        System.out.println(isPalindrome(number));
    }

    private static boolean isPalindrome(int[] number) {
        boolean result = false;
        for (int i = 0; i < number.length; i++) {
            if (number[i] == number[number.length - i - 1]) {
                result = true;
                break;
            }
        }
        return result;
    }
}
