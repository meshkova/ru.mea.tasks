package ru.mea.task_13;

import java.util.Scanner;

public class Task13 {
    private static Scanner scanner = new Scanner(System.in);
    public static void main(String[] args) {

        System.out.println("Введите символ");
        String str = scanner.nextLine();
        char symbol = str.charAt(0);
        if (Character.isDigit(symbol)) System.out.println("Вы ввели цифру");
        if (Character.isLetter(symbol)) System.out.println("Вы ввели букву");
        if (".,:;!?/".contains(str)) System.out.println("Вы ввели знак пунктуации");

    }
}
